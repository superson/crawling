 Every Review Crawler
=====================


## 최상위 디렉토리

> 개발 설정 관련 파일

    .
    ├── crawling                   # 크롤러 실행 모듈 디렉토리
    ├── redis-stable               # redis 라이브러리
    ├── gitignore                  
    ├── gitlab-ci.yml              # 깃랩의 ci 설정 파일
    ├── README.md                  
    ├── docker file                # 도커 빌드 설정 파일
    └── requirements.txt           # 사용된 파이썬 모듈 모음

##### 사용한 python 모듈

  * amqp==2.5.0
  * atomicwrites==1.3.0
  * attrs==19.1.0
  * billiard==3.6.0.0
  * celery==4.3.0
  * certifi==2019.6.16
  * chardet==3.0.4
  * idna==2.8
  * importlib-metadata==0.19
  * kombu==4.6.3
  * lxml==4.4.0
  * more-itertools==7.2.0
  * packaging==19.1
  * pluggy==0.12.0
  * psycopg2==2.8.3
  * py==1.8.0
  * pyparsing==2.4.2
  * pytest==5.0.1
  * pytz==2019.2
  * redis==3.3.11
  * requests==2.22.0
  * six==1.12.0
  * SQLAlchemy==1.3.6
  * urllib3==1.25.3
  * vine==1.3.0
  * wcwidth==0.1.7
  * zipp==0.5.2
  * beautifulsoup4==4.7.1
  * Flask==1.1.1
  * waitress==1.3.1

## crawler 디렉토리

> 크롤러 모듈 파일  

    crawler
    ├── app.py                     # celery, flask 모듈
    ├── category                   # 카테고리별 세부 크롤러 모듈 모음 디렉토리
    ├── celery_setting.py          # python celery beat 설정 파일
    ├── crawler.py                 # 크롤러 모듈
    ├── db                         # 데이터 베이스 세팅 디렉토리
    ├── executor.py                # 크롤러 모듈 조합한 쿼리 모듈
    ├── input                      # 기반 input 정보 모음 디렉토리 (도커에서 링크시켜 사용)
    ├── log                        # 크롤링 과정 log 기록 디렉토리
    ├── site                       # 크롤링에 필요한 사이트별 파일 모음 디렉토리
    ├── task.py                    # 스케줄링에 필요한 celery task 파일
    ├── word.py                    # 워드 클라우드 모듈
    └── test_crawler.py            # 크롤러 모듈 test 파일


## category 디렉토리

> 크롤러 세부 모듈 파일

    category                       
    ├── movie                      # 영화 카테고리 세부 모듈
    │   ├── movie_module.py        # 영화 정보 크롤러 모듈
    │   └── movie_review.py        # 영화 리뷰 크롤러 모듈
    └── restaurant                 # 음식점 카테고리 세부 모듈
        ├── naver_pid.py           # 네이버 음식점 크롤링 selenium 모듈
        ├── restaurant_module.py   # 음식점 정보 크롤러 모듈
        └── restaurant_review.py   # 음식점 리뷰 크롤러 모듈     


#### site_list.json 예시
*  naver movie에 대해서 크롤링에 필요한 xpath들을 모아서 json 폼으로 관리한다.

~~~
{
    "site_name" : "naver_movie",
    "scale_type" : "10",
    "base_url" : "https://movie.naver.com",
    "search_url" : "https://movie.naver.com/movie/search/result.nhn?query=<TITLE>&section=all&ie=utf8",
    "title_xpath" : "//*[@id=\"old_content\"]/ul[2]/li[not(@*)]/dl/dt/a",
    "identify_xpath" : "//*[@id=\"old_content\"]/ul[2]/li[not(@*)]/dl/dd[2]",
    "score_xpath" : "//*[@id=\"content\"]/div[1]/div[4]/div[5]/div[2]/div[2]/div[1]/div/div/em",
    "actor_xpath" : "//*[@id=\"content\"]/div[1]/div[4]/div[2]/div/ul/li[not(@*)]/a[2]",
    "poster_xpath" : "//*[@id=\"content\"]/div[1]/div[2]/div[2]/a/img",
    "summary_xpath" : "//*[@id=\"content\"]/div[1]/div[4]/div[1]/div/div[1]/p"
}
~~~

## db 디렉토리

> db 설정 및 테이블 스키마 정의

    db
    ├── db_setting.py                   # 데이터 베이스 세팅 파일
    └── models.py                       # 테이블 스키마 정의


#### Rawdata, RefinedData의 스키마
* Rawdata의 data에 크롤링한 html 자체가 저장된다.
* review data table에 파싱된 리뷰 정보들이 저장된다.
* RefinedData의 data에는 추출된 자료가 정리되어 json 으로 저장된다.

~~~
class Rawdata(Base):
    __tablename__ = 'rawdata_movie'

    uuid = Column(UUID(as_uuid=True), primary_key=True)
    date = Column(DateTime(timezone=True), default=func.now(), primary_key=True)
    source_site = Column(String(20), primary_key=True)
    source_id = Column(String(32))
    data = Column(String)
    recovery = Column(String)

class Review_movie(Base):
    __tablename__ = 'review_movie'

    review_id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True))
    source_site = Column(String(20), primary_key=True)
    source_id = Column(String(32), primary_key=True)
    user_name = Column(String(20))
    date = Column(String(32))
    review = Column(String)
    rating = Column(String(10))

class RefinedData(Base):
    __tablename__ = 'wiki_refineddata'

    uuid = Column(UUID(as_uuid=True), primary_key=True)
    page_id = Column(Integer, nullable=True)
    update_date = Column(DateTime(timezone=True))
    data = Column(JSONB)
~~~
