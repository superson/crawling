from celery import Celery
from celery.schedules import crontab

CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

app = Celery('celery_setting', broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)

app.conf.update(
    CELERY_ENABLE_UTC=True,
    CELERYBEAT_SCHEDULE={
        'movie_schedule': {
            'task': 'task.movie_schedule',
            'schedule': crontab(0, 0, day_of_month='1'),
            'args': ()
        },
        'restaurant_schedule': {
            'task': 'task.restaurant_schedule',
            'schedule': crontab(0, 0, day_of_month='10'),
            'args': ()
        },
    }
)
