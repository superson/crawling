import pytest
import crawler
from category.movie import movie_module

@pytest.fixture(scope='module')
def movie():
    return '000|&|엑시트|&|한국|&|2019|&|2018'


@pytest.fixture(scope='module')
def watcha():
    site = crawler.read_site('movie','watcha')
    return site


@pytest.fixture(scope='module')
def naver():
    site = crawler.read_site('movie','naver_movie')
    return site


@pytest.fixture(scope='module')
def daum():
    site = crawler.read_site('movie','daum_movie')
    return site


@pytest.fixture(scope='module')
def maxmovie():
    site = crawler.read_site('movie','maxmovie')
    return site


@pytest.fixture(scope='module')
def header():
    return crawler.get_header()


def test_get_url_watcha(watcha, movie):
    watcha_url = 'https://watcha.com/ko-KR/contents/mdErj22'
    url = crawler.get_url('movie', watcha, movie)
    assert watcha_url == url


def test_get_url_naver(naver, movie):
    naver_url = 'https://movie.naver.com/movie/bi/mi/basic.nhn?code=174903'
    url = crawler.get_url('movie', naver, movie)
    assert naver_url == url


def test_get_url_daum(daum, movie):
    daum_url = 'http://movie.daum.net/moviedb/main?movieId=121137'
    url = crawler.get_url('movie', daum, movie)
    assert daum_url == url


def test_get_url_maxmovie(maxmovie, movie):
    maxmovie_url = 'https://www.maxmovie.com/Movie/M000106709'
    url = crawler.get_url('movie', maxmovie, movie)
    assert maxmovie_url == url

# poster는 후에 모듈 수정 시 검증 방법을 달리함


def test_getdata_watcha(watcha, header):
    import requests
    from lxml import html
    watcha_url = 'https://watcha.com/ko-KR/contents/mdErj22'

    page = requests.get(watcha_url, headers=header)
    tree = html.fromstring(page.content)

    score_xpath = watcha['score_xpath']
    score = movie_module.get_score(tree, score_xpath)
    assert score

    actor_xpath = watcha['actor_xpath']
    actor = movie_module.get_actor(tree, actor_xpath)
    assert actor

    summary_xpath = watcha['summary_xpath']
    summary = movie_module.get_summary(tree, summary_xpath)
    assert summary


def test_getdata_naver(naver, header):
    import requests
    from lxml import html
    naver_url = 'https://movie.naver.com/movie/bi/mi/basic.nhn?code=174903'
    page = requests.get(naver_url, headers=header)
    tree = html.fromstring(page.content)

    score_xpath = naver['score_xpath']
    score = movie_module.get_score(tree, score_xpath)
    assert score

    actor_xpath = naver['actor_xpath']
    actor = movie_module.get_actor(tree, actor_xpath)
    assert actor

    poster_xpath = naver['poster_xpath']
    poster = movie_module.get_poster(tree, poster_xpath)
    assert poster

    summary_xpath = naver['summary_xpath']
    summary = movie_module.get_summary(tree, summary_xpath)
    assert summary


def test_getdata_daum(daum, header):
    import requests
    from lxml import html
    daum_url = 'http://movie.daum.net/moviedb/main?movieId=121137'

    page = requests.get(daum_url, headers=header)
    tree = html.fromstring(page.content)

    score_xpath = daum['score_xpath']
    score = movie_module.get_score(tree, score_xpath)
    assert score

    actor_xpath = daum['actor_xpath']
    actor = movie_module.get_actor(tree, actor_xpath)
    assert actor

    summary_xpath = daum['summary_xpath']
    summary = movie_module.get_summary(tree, summary_xpath)
    assert summary


def test_getdata_maxmovie(maxmovie, header):
    import requests
    from lxml import html
    maxmovie_url = 'https://www.maxmovie.com/Movie/M000106709'

    page = requests.get(maxmovie_url, headers=header)
    tree = html.fromstring(page.content)

    score_xpath = maxmovie['score_xpath']
    score = movie_module.get_score(tree, score_xpath)
    assert score

    actor_xpath = maxmovie['actor_xpath']
    actor = movie_module.get_actor(tree, actor_xpath)
    assert actor


    summary_xpath = maxmovie['summary_xpath']
    summary = movie_module.get_summary(tree, summary_xpath)
    assert summary
