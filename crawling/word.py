from konlpy.tag import Twitter
from collections import Counter

from db import db_setting
from db.models import RefinedData, Review_movie

import logging
def log_init(start_date):
    logging.basicConfig(filename='./log/'+start_date+'.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S')

def get_tag_list(sentences_list):

    okt = Twitter()
    sentences_tag = []
        #형태소 분석하여 리스트에 넣기
    for sentence in sentences_list:
        sentence = sentence[0]
        morph = okt.pos(sentence)
        sentences_tag.append(morph)

    noun_adj_list = []
        #명사와 형용사만 구분하여 이스트에 넣기
    for sentence1 in sentences_tag:
        for word, tag in sentence1:
            if tag in ['Noun', 'Adjective']:
                noun_adj_list.append(word)
    counts = Counter(noun_adj_list)
    tags = counts.most_common(8)
    print(tags)

def get_review_data(category):
    log_init('word')
    rawdata_engine = db_setting.get_rawdata_engine()
    rawdata_db_session = db_setting.get_sesstion(rawdata_engine)
    uuid_list = list(rawdata_db_session.query(Review_movie.uuid).group_by(Review_movie.uuid).all())
    for uuid in uuid_list:
        data_list = rawdata_db_session.query(Review_movie.review).filter(Review_movie.uuid == uuid).all()
        get_tag_list(data_list)
        break
    rawdata_db_session.close()

get_review_data('movie')
