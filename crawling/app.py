from flask import Flask, request
from waitress import serve
from celery import Celery
import executor

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

@celery.task
def data_update_async(uuid, category):  # 비동기로 들어오는 특정 터플에 대한 rawdata추가 및 refine 데이터의 업데이트 수행
    identify = executor.get_identify_use_uuid(uuid, category)
    if identify == None:
        return ('identify is null...')
    else :
        source = 'data_update_async'
        identify_list = []
        identify_list.append(identify)
        input = [category, source, identify_list]
        executor.insert_rawdata(input)
        uuid_list = [(uuid)]
        input = [category, source, uuid_list]
        executor.update_refined(input)
        str = uuid+'in '+category+' upadate finish!'
        return str

@app.route('/update', methods=['POST'])
def update():
    uuid = request.form['uuid']
    category = request.form['category']
    token = request.form['token']
    if token == 'E4j[uWqq~60&kf_Oe3CH':
        ret = data_update_async.delay(uuid,category)
        return 'sucess'
    else :
        return 'fail'

@app.route('/')
def index():
    return 'server is working'

if __name__ == '__main__':
    # app.run()
    serve(app, host='0.0.0.0', port=9876)

# celery  세팅 다시
# 웹 ui 꾸미기
# 음식 댓글 추가
