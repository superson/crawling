from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


def get_rawdata_engine():
    # engine = create_engine('postgresql://superson:]43Z6)GqPbhGX?7D@superson:5432/crawling', echo=True)
    engine = create_engine('postgresql://superson:]43Z6)GqPbhGX?7D@15.164.164.217:51492/crawling', echo=True)

    return engine


def get_refineddata_engine():
    # engine = create_engine('postgresql://superson:]43Z6)GqPbhGX?7D@superson:5432/everyreview', echo=True)
    engine = create_engine('postgresql://superson:]43Z6)GqPbhGX?7D@15.164.164.217:51492/everyreview', echo=True)

    return engine


def get_sesstion(engine):
    Session = sessionmaker(engine)
    db_session = Session()
    return db_session


def create_rawdata_table(engine):
    import db.models
    Base.metadata.tables["rawdata_movie"].create(bind=engine)

def create_rawdata_table_restaurant(engine):
    import db.models
    Base.metadata.tables["rawdata_restaurant"].create(bind=engine)

def create_review_table(engine):
    import db.models
    Base.metadata.tables["review_movie"].create(bind=engine)

def create_review_table_restaurant(engine):
    import db.models
    Base.metadata.tables["review_restaurant"].create(bind=engine)
