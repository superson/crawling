import requests
from lxml import html
import json
from time import sleep
import random
import re  # regular expression

# class Crawler:
#     def __init__(self, policy):
#         self.policy = policy
#         self.session = requests.Session()
#
#     def request(self, url, method='GET', params={}):
#         header = self.policy.header()
#         resp = self.session.request(url, method=method, params=params)
#         sleep(self.policy.delay())
#         return resp
def get_header():
    headers = {
        'user-agent': 'user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    return headers

def get_naver_review(source_id):
    naver_url = 'https://movie.naver.com/movie/bi/mi/review.nhn?code=<SOURCE_ID>&page=<INDEX>'
    detail_url = 'https://movie.naver.com/movie/bi/mi/reviewread.nhn?nid=<NID>&code=<SOURCE_ID>&order=#tab'
    naver_url = naver_url.replace('<SOURCE_ID>', source_id)
    detail_url = detail_url.replace('<SOURCE_ID>', source_id)

    total_xpath = '//*[@id="reviewTab"]/div/div/div[2]/span/em/text()'
    li_xpath = '//*[@id="reviewTab"]/div/div/ul/li[not(@*)]'

    total_url = naver_url.replace('<INDEX>', '1')
    page = requests.get(total_url, headers=get_header())
    tree = html.fromstring(page.content)
    total = tree.xpath(total_xpath)
    try:
        total = total[0]
        total = int(total)//10+2
    except:
        total = 0
    review_list = []
    for index in range(1, total):
        sleep(1 + random.random())
        url = naver_url.replace('<INDEX>', str(index))
        page = requests.get(url, headers=get_header())
        tree = html.fromstring(page.content)
        li_elem = tree.xpath(li_xpath)
        for li in li_elem:
            anchor = li.xpath('a')
            a = anchor[0].attrib['onclick']
            nid = re.findall("\d+", a)
            nid = str(nid[0])
            d_url = detail_url.replace('<NID>', nid)
            page = requests.get(d_url, headers=get_header())
            tree = html.fromstring(page.content)

            user_name = tree.xpath('//*[@id="content"]/div[1]/div[4]/div[1]/div[3]/ul/li[not(@*)]/a/em')
            review = tree.xpath('//*[@id="content"]/div[1]/div[4]/div[1]/div[4]')
            date = tree.xpath('//*[@id="content"]/div[1]/div[4]/div[1]/div[2]/span')
            rating = tree.xpath('//*[@id="content"]/div[1]/div[4]/div[1]/div[2]/div/em')

            try:
                user_name = user_name[len(user_name)-1].text_content()
                review = review[0].text_content()
                review = review.strip()
                date = date[0].text_content()
                rating = rating[0].text_content()
            except:
                continue

            tmp = [user_name, review, date, rating]
            review_list.append(tmp)
    return review_list

    # print('user : ',user_name[len(user_name)-1].text_content())
    # print('review : ',review[0].text_content())
    # print('date : ',date[0].text_content())
    # try:
    #     print('rating : ',rating[0].text_content())
    # except:
    #     print('rating : none')
    # print('************************************************************\n')


def ajax_header(refer):
    headers = {'accept': 'application/vnd.frograms+json;version=20',
               'Origin': 'https://watcha.com',
               'Referer': refer,
               'Sec-Fetch-Mode': 'cors',
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
               'x-watcha-client': 'watcha-WebApp',
               'x-watcha-client-language': 'ko',
               'x-watcha-client-region': 'KR',
               'x-watcha-client-version': '1.0.0',
               'x-watcha-remote-addr': '121.140.163.199'}
    return headers


# def ajax_param(page):
#     param = {'default_version': '20',
#              'filter': 'all',
#              'order': 'popular',
#              'page': page,
#              'size': '3',
#              'vendor_string': 'frograms'}
#     return param


def get_watcha_review(source_id):
    # refer = 'https://watcha.com/ko-KR/contents/mdErj22/comments' #엑시트 댓글 담고있는 주소
    # ajax_url = 'https://api.watcha.com/api/contents/mdErj22/comments?default_version=20&filter=all&order=popular&page=<INDEX>&size=3&vendor_string=frogram'

    refer = 'https://watcha.com/ko-KR/contents/{source_id}/comments'
    ajax_url = 'https://api.watcha.com/api/contents/{source_id}/comments?default_version=20&filter=all&order=popular&page=1&size=3&vendor_string=frograms'

    refer = refer.format(source_id=source_id)
    ajax_url = ajax_url.format(source_id=source_id)

    review_list = []
    while True:
        response = requests.get(ajax_url, headers=ajax_header(refer)).text
        json_data = json.loads(response)
        result_part = json_data['result']
        next_uri = result_part['next_uri']
        result_list = result_part['result']
        for result in result_list:
            user = result['user']['name']
            user_name = result['user']['name']
            review = result['text']
            review = review.strip()
            date = result['created_at']
            rating = result['user_content_action']['rating']
            tmp = [user_name, review, date, rating]
            review_list.append(tmp)
        if next_uri == None:
            break
        else :
            ajax_url = 'https://api.watcha.com'+next_uri
        sleep(1.3 + random.random())
    return review_list

def get_total(total_string):
    total = ''
    for c in total_string:
        if c == ')':
            break
        if '0' <= c <= '9':
            total += c
    return total

def get_daum_review(source_id):
    daum_url = 'https://movie.daum.net/moviedb/grade?movieId={source_id}&type=netizen&page=<INDEX>'
    daum_url = daum_url.format(source_id=source_id)

    total_xpath = '//*[@id="mArticle"]/div[2]/div[2]/div[1]/div[1]'
    li_xpath = '//*[@id="mArticle"]/div[2]/div[2]/div[1]/ul/li[@*]'

    total_url = daum_url.replace('<INDEX>', '1')
    page = requests.get(total_url, headers=get_header())
    tree = html.fromstring(page.content)
    total_elem = tree.xpath(total_xpath)
    try:
        total_str = total_elem[0].text_content()
        total_str = get_total(total_str)
    except:
        total = 0
    else:
        total = int(total_str)//10 + 2

    review_list = []
    for index in range(1, total):
        sleep(1 + random.random())
        url = daum_url.replace('<INDEX>', str(index))
        page = requests.get(url, headers=get_header())
        tree = html.fromstring(page.content)
        li_elem = tree.xpath(li_xpath)
        for li in li_elem:
            user_name = li.xpath('div/a[1]/em')
            review = li.xpath('div/p')
            date = li.xpath('div/div[2]/span[1]')
            rating = li.xpath('div/div[1]/em')
            try:
                user_name = user_name[0].text_content()
                user_name = user_name[:20]
                review = review[0].text_content()
                review = review.strip()
                date = date[0].text_content()
                date = date.strip()
                rating = rating[0].text_content()
            except:
                continue
            tmp = [user_name, review, date, rating]
            review_list.append(tmp)
    return review_list


def get_maxmovie_review(source_id):
    max_url = 'http://www.maxmovie.com/Movie/{source_id}/talk?size=10&no=<INDEX>'
    max_url = max_url.format(source_id=source_id)

    total_xpath = '//*[@id="content"]/div[1]/div[2]/div[3]/div[2]/div/ul/li[1]/a/em'
    li_xpath = '//*[@id="content"]/div[1]/div[2]/div[3]/div[2]/ul/li[@*]'

    total_url = max_url.replace('<INDEX>', '1')
    page = requests.get(total_url, headers=get_header())
    tree = html.fromstring(page.content)
    total_elem = tree.xpath(total_xpath)
    try:
        total_str = total_elem[0].text_content()
    except:
        total = 0
    else:
        total = int(total_str)//10 + 2

    review_list = []
    for index in range(1, total):
        url = max_url.replace('<INDEX>', str(index))
        page = requests.get(url, headers=get_header())
        tree = html.fromstring(page.content)
        li_elem = tree.xpath(li_xpath)
        for li in li_elem:
            user_name = li.xpath('div/div/p[1]/text()')
            review = li.xpath('div/div/p[2]')
            date = li.xpath('div/div/p[1]/span')
            rating = li.xpath('div/p/span[2]')

            try:
                user_name = user_name[0].strip()
                review = review[0].text_content()
                review = review.strip()
                date = date[0].text_content()
                rating = rating[0].text_content()
            except:
                continue
            tmp = [user_name, review, date, rating]
            review_list.append(tmp)
        sleep(1 + random.random())
    return review_list
