import requests
import json
from lxml import html
import random
from time import sleep
from collections import OrderedDict  # for make json

class MovieInput:
    def __init__(self, json):
        self.json = json
    @property
    def cid(self):
        return self.json['movieCd']
    @property
    def title(self):
        return self.json['movieNm']
    @property
    def country(self):
        return self.json['repNationNm']
    @property
    def open_year(self):
        raw = self.json['openDt']
        if len(raw) == 8:
            return raw[:4]
        else:
            return raw
    @property
    def start_year(self):
        raw = self.json['prdtYear']
        if len(raw) == 1:
            raw = ''
        else:
            raw = raw[:4]
        return raw

def movie_input_api(index):
    # url_format = 'http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieList.json?key=430156241533f1d058c603178cc3ca0e&curPage={}'
    # url = url_format.format(index)
    url = f'http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieList.json?key=430156241533f1d058c603178cc3ca0e&curPage={index}'
    response = requests.get(url)
    json_dummy = response.json()
    movie_list = json_dummy['movieListResult']['movieList']
    input_list = []
    for movie in movie_list:
        item = MovieInput(movie)
        tmp = '|&|'.join([item.cid, item.title, item.country, item.open_year, item.start_year])
        input_list.append(tmp)
    sleep(1 + random.random())
    return input_list

# def read_input(category, file_name): backup for read local module
#     input_list = []
#     path = f'input/{category}/{file_name}.txt'
#     # path = 'input/{category}/{file_name}.txt'.format(category=category, file_name=file_name)
#     with open(path, mode='r', encoding='utf-8') as f:
#         while True:
#             line = f.readline()
#             if not line:
#                 break
#             _, cid, title, contry, open_year, start_year = line.split('|')
#             if len(open_year) == 8:
#                 open_year = open_year[:4]
#             if len(start_year) == 1:
#                 start_year = ''
#             else:
#                 start_year = start_year[:4]
#             tmp = '|'.join([cid, title, contry, open_year, start_year])
#             input_list.append(tmp)
#     return input_list

def get_movie_url(site_data, input_data):
    url = site_data['search_url']
    title_xpath = site_data['title_xpath']
    identify_xpath = site_data['identify_xpath']

    _, title, contry, open_year, start_year = input_data.split('|&|')
    url = url.replace("<TITLE>", title)
    max = get_max(contry, open_year, start_year)

    page = requests.get(url, headers=get_header())
    tree = html.fromstring(page.content)

    titles = tree.xpath(title_xpath)
    identifys = tree.xpath(identify_xpath)

    candidate_list = []
    for i in range(0, len(titles)):
        candidate_title = titles[i].text_content()
        candidate_data = identifys[i].text_content()
        match_point = get_match(input_data, candidate_title, candidate_data)
        elem = (i, match_point)
        candidate_list.append(elem)

    if len(candidate_list) > 0:
        candidate_list.sort(key=lambda t: t[1], reverse=True)
        index = candidate_list[0][0]
        match = candidate_list[0][1]
    else:
        return None    

    if match < max:
        if not(len(candidate_list) == 1 and match == max-1):
            raise match_error('match cnt is not full')

    if site_data['site_name'] == 'watcha':
        content_url = titles[index].getparent(
        ).getparent().getparent().attrib['href']
    else:
        content_url = titles[index].attrib['href']
    content_url = make_full_url(site_data, content_url)
    return content_url

def get_header():
    headers = {
        'user-agent': 'user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    return headers

def get_max(contry, open_year, start_year):
    max = 3
    if contry == '':
        max -= 1
    if open_year == '' or start_year == '':
        max -= 1
    return max

def get_match(identify, candidate_title, candidate_data):
    if (len(candidate_title) + len(candidate_data)) > 90:
        return 0
    cnt = 0
    _, title, contry, open_year, start_year = identify.split('|&|')
    if title in candidate_title:
        cnt += 1
    if open_year in candidate_data or start_year in candidate_data:  # 개봉 또는 제작년도에 일치하는 년도 있으면 추가
        cnt += 1
    if contry in candidate_data:
        cnt += 1
    else:
        cnt = 0
    return cnt

def make_full_url(site, content_url):
    base_url = site['base_url']
    content_url = base_url + content_url
    return content_url


#**********#
def make_movie_json(uuid, list):
    import ast
    poster = ''
    max_summary = ''
    max_actor = [[], []]
    title = ''

    site_info_list = []
    for tup in list:
        source_site = tup[0]  # naver, daum..
        rawdata = tup[1]  # html
        recovery = tup[2]
        recovery = ast.literal_eval(recovery)  # string form list ro list

        category = recovery[0]
        identify = recovery[1]
        url = recovery[3]
        site = recovery[4]

        score, actor_list, poster_url, summary = get_data(rawdata, site)
        try:
            _, title, contry, open_year, start_year = identify.split('|')
        except:
            _, title, contry, open_year, start_year = identify.split('|&|')

        if source_site == 'naver_movie':
            poster = poster_url

        max_summary = get_max_summary(summary, max_summary)
        max_actor = get_max_actor(actor_list, max_actor)

        tmp = OrderedDict()
        tmp['site_name'] = source_site
        tmp['url'] = url
        tmp['rating'] = score
        site_info_list.append(tmp)

    info_part = OrderedDict()  # poster, actor, summary
    info_part['poster_url'] = poster
    info_part['director'] = max_actor[0]
    info_part['actor'] = max_actor[1]
    info_part['summary'] = max_summary

    data_part = OrderedDict()
    data_part['uuid'] = uuid
    data_part['category'] = category
    data_part['title'] = title
    data_part['info'] = info_part

    json_form = OrderedDict()
    json_form['data'] = data_part
    json_form['site'] = site_info_list
    j = json.dumps(json_form, ensure_ascii=False)
    return json.loads(j)


def get_max_summary(summary, max):
    if len(summary) > len(max):
        max = summary
    return max


def get_max_actor(actor, max):
    if len(actor[1]) > len(max[1]):
        max = actor
    return max


def get_data(page, site_data):
    tree = html.fromstring(page)

    score_xpath = site_data['score_xpath']
    score_scale = site_data['scale_type']
    score = get_score(tree, score_xpath)
    score = extract_score(score)
    score = score_scaling(score, score_scale)

    actor_xpath = site_data['actor_xpath']
    actor = get_actor(tree, actor_xpath)

    poster_xpath = site_data['poster_xpath']
    poster = get_poster(tree, poster_xpath)

    summary_xpath = site_data['summary_xpath']
    summary = get_summary(tree, summary_xpath)
    summary = summary.strip()

    return score, actor, poster, summary


def get_score(tree, score_xpath):
    score = tree.xpath(score_xpath)
    try:
        score = score[0].text_content()
    except:
        score = '0.0'

    return score


def extract_score(s):
    score = s
    for i in range(len(s)):
        if s[i] == '\n':
            break
        if s[i] == '.':
            score = s[i-1:i+2]
    return score


def score_scaling(score, scale):
    tmp = score
    try:
        if scale == '5':
            score = float(score)*2.0
    except:
        score = tmp
    return str(score)

# 감독 파싱하기


def get_actor(tree, actor_xpath):
    actor = tree.xpath(actor_xpath)
    list = extract_actor(actor)
    if len(list) > 0:
        cnt = get_director_cnt(actor)
        director_list = list[0:cnt]
        actor_list = list[cnt:]
        list = [director_list, actor_list]
    else:
        list = [[], []]
    return list


def extract_actor(a):
    actor_list = []
    for actor in a:
        actor_list.append(actor.text_content())
    if len(actor_list) > 0 and (actor_list[0] == '왓챠플레이' or actor_list[0] == 'TVING'):
        actor_list = actor_list[1:]
    return actor_list


def get_director_cnt(a):
    director_cnt = 0
    node = a[-1]
    while True:
        if 'ul' in str(node):
            all_info = node.text_content()
            director_cnt = all_info.count('감독')
            break
        node = node.getparent()
    return director_cnt


def remove_poster_query(url):
    for i in range(len(url)-1, 0, -1):
        if url[i] == '?':
            break
    url = url[:i]
    return url

def get_poster(tree, poster_xpath):
    poster = tree.xpath(poster_xpath)
    try:
        poster = poster[0].attrib['src']
        poster = remove_poster_query(poster)
    except:
        poster = None
    return poster


def get_summary(tree, summary_xpath):
    summary = tree.xpath(summary_xpath)
    try:
        summary = summary[0].text_content()
    except:
        summary = None
    return summary
