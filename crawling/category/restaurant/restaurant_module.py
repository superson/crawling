import csv
import requests
import json
from lxml import html
import random
from time import sleep
import time
from collections import OrderedDict  # for make json
from bs4 import BeautifulSoup
# 0 uid, 1영업상태구분코드(1이어야영업), 2전화, 3주소, 4도로명, 5가게이름, 6갱신일자, 7한식,경양식 등

def get_header():
    headers = {
        'user-agent': 'user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    return headers
    # 'X-Naver-Client-Id' : 'A9ukCxTDMSkVKn9v_tsb',
    # 'X-Naver-Client-Secret' : 'gHMTylftOT'

def restaurant_input_api(index):
    f = open('/app/crawling/input/food/refine'+str(index)+'.csv', 'r', encoding='utf-8')
    data = csv.reader(f)
    data = list(data)
    f.close()
    return data


def get_match(title, address, candidate_title, candidate_data):
    candidate_title = candidate_title.strip()
    candidate_data = candidate_data.strip()

    for i in range(len(candidate_title)):
        if candidate_title[i] == '.':
            break
    candidate_title = candidate_title[i+2:]
    cnt = 0
    candidate_title_list = candidate_title.split(' ')
    for elem in candidate_title_list:
        if elem in title:
            cnt += 1

    identify_list = candidate_data.split(' ')
    for elem in identify_list:
        if elem in address:
            cnt += 1
    return cnt

def mango_match(title, address, candidate_title, candidate_url, identify_xpath):
    page = requests.get(candidate_url, headers=get_header())
    tree = html.fromstring(page.content)
    identifys = tree.xpath(identify_xpath)
    identify = identifys[0].text_content()

    identify = identify.strip()
    candidate_title = candidate_title.strip()
    cnt = 0
    if candidate_title in title:
        cnt += 1

    identify_list = identify.split(' ')
    for elem in identify_list:
        if elem in address:
            cnt += 1
    # print('title : ',candidate_title)
    # print('address : ',identify)
    # print('cnt : ',cnt)
    return cnt

def get_kakao_url(tree):
    anchor_xpath = '//*[@id="poiColl"]/div[2]/div/div[2]/div[1]/a'
    a = tree.xpath(anchor_xpath)
    try:
        return a[0].attrib['href']
    except:
        return None

def get_naver_url(title, address):
    url = f'https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query={title}'
    title_xpath = '//*[@id="sp_local_1"]'
    res = requests.get(url,headers=get_header()).text
    # f = open('tttt.html','w',encoding='utf-8')
    # f.write(res)
    # f.close()
    soup = BeautifulSoup(res, 'html.parser')
    ul_part = soup.findAll('ul', {'class' : 'lst_map'})
    pid = ''
    if len(ul_part) > 0:
        tree = html.fromstring(str(ul_part))
        li_list = tree.xpath('//*[@class="lst_map"]/li[@*]/dl/dd[2]/span[2]')
        a_list = tree.xpath('//*[@class="lst_map"]/li[@*]/dl/dt/a')
        candidate_list = []
        for i in range(len(li_list)):
            candidate_data = li_list[i].text_content()
            candidate_title = a_list[i].text_content()
            match_point = get_match(title, address, candidate_title, candidate_data)
            if match_point > 3:
                elem = (i, match_point)
                candidate_list.append(elem)
            else:
                continue
        if len(candidate_list) > 0:
            candidate_list.sort(key=lambda t: t[1], reverse=True)
            index = candidate_list[0][0]
            match = candidate_list[0][1]
        else:
            return None
        a_url = a_list[index].attrib['href']
        s = a_url.find('?code=')
        pid = a_url[s+6:]
    else :
        div_part = soup.findAll('div', {'id' : 'place_main_ct'})
        if len(div_part) > 0:
            tree = html.fromstring(str(div_part))
            anchor = tree.xpath('//*[@id="place_main_ct"]/div/div/div[1]/div[2]/div[1]/div/div/a')
            if len(anchor) > 0:
                a_url = anchor[0].attrib['href']
                s = a_url.find('id=')
                for i in range(s+3,len(a_url)):
                    if a_url[i] == '&':
                        break
                    pid += a_url[i]
            else :
                div_part = soup.findAll('div', {'class' : 'biz_name_area'})
                anchor = tree.xpath('//*[@class="biz_name_area"]/a')
                if len(anchor) > 0:
                    a_url = anchor[0].attrib['href']
                    s = a_url.find('id=')
                    for i in range(s+3,len(a_url)):
                        if a_url[i] == '&':
                            break
                        pid += a_url[i]
                else :
                    return None
        else :
            return None

    content_url = 'https://store.naver.com/restaurants/detail?id='+pid
    return content_url

def get_restaurant_url(site_data, input_data):
    # url = site_data['search_url']
    # title_xpath = site_data['title_xpath']
    # identify_xpath = site_data['identify_xpath']
    # a_xpath = site_data['a_xpath']
    # base_url = site_data['base_url']

    # url = "https://www.diningcode.com/isearch.php?query=<TITLE>"
    # title_xpath = "//*[@id=\"div_rn\"]/ul/li[not(@*)]/a/span[1]"
    # identify_xpath  = "//*[@id=\"div_rn\"]/ul/li[not(@*)]/a/span[3]"
    # a_xpath = '//*[@id=\"div_rn\"]/ul/li[not(@*)]/a'
    # base_url = 'https://www.diningcode.com'

    # site_name = 'mangoplate'
    # url = "https://www.mangoplate.com/search/<TITLE>"
    # title_xpath = "/html/body/main/article/div[2]/div/div/section/div[3]/ul/li[@*]/div[@*]/figure/figcaption/div/a/h2"
    # identify_xpath = "/html/body/main/article/div[1]/div[1]/div/section[1]/table/tbody/tr[1]/td"
    # a_xpath = "/html/body/main/article/div[2]/div/div/section/div[3]/ul/li[@*]/div[@*]/figure/a"
    # base_url = "https://www.mangoplate.com"

    # site_name = 'kakao_map'
    # url = 'https://search.daum.net/search?w=tot&DA=YZR&t__nil_searchbox=btn&sug=&sugo=&q=<TITLE>'
    # title_xpath = '//*[@id="poiColl"]/div[2]/div[3]/ul/li[not(@*)]/div[1]/div/a[1]'
    #
    # identify_xpath = '//*[@id="poiColl"]/div[2]/div[3]/ul/li[not(@*)]/div[1]/div/div[1]/div/a'
    # a_xpath = '//*[@id="poiColl"]/div[2]/div[3]/ul/li[not(@*)]/div[1]/div/a[1]'
    # base_url = ''


    # title = input_data[5]
    # address = input_data[3] #지번
    #
    # if site_name == 'naver_restaurant':
    #     url = url.replace("<TITLE>", title)
    #     content_url = get_naver_restaurant_url(title, address, url, base_url)
    #     return content_url

    title = input_data[5]
    address = input_data[3] #지번

    site_name = site_data['site_name']
    url = site_data['search_url']
    title_xpath = site_data['title_xpath']
    identify_xpath = site_data['identify_xpath']
    a_xpath = site_data['a_xpath']
    base_url = site_data['base_url']

    if site_name == 'naver_map':
        if len(input_data[4]) > 0:
            address = input_data[4]
        content_url = get_naver_url(title,address)
        return content_url

    url = url.replace("<TITLE>", title)
    page = requests.get(url, headers=get_header())
    tree = html.fromstring(page.content)

    titles = tree.xpath(title_xpath)
    identifys = tree.xpath(identify_xpath)
    anchor = tree.xpath(a_xpath)

    # print('len of titles : ',len(titles))
    # print('len of identify : ',len(identifys))
    if site_name == 'kakao_map' and len(titles) == 0:
        content_url = get_kakao_url(tree)
        return content_url


    candidate_list = []
    for i in range(0, len(titles)):
        candidate_title = titles[i].text_content()

        if site_name == 'mangoplate':
            candidate_url = base_url + anchor[i].attrib['href']
            match_point = mango_match(title, address, candidate_title, candidate_url, identify_xpath)
        elif site_name == 'kakao_map':
            if len(input_data[4]) > 0:
                address = input_data[4]
            candidate_data = identifys[i].text_content()
            match_point = get_match(title, address, candidate_title, candidate_data)
        else :
            candidate_data = identifys[i].text_content()
            match_point = get_match(title, address, candidate_title, candidate_data)


        if match_point > 2:
            elem = (i, match_point)
            candidate_list.append(elem)
        else:
            continue

    if len(candidate_list) > 0:
        candidate_list.sort(key=lambda t: t[1], reverse=True)
        index = candidate_list[0][0]
        match = candidate_list[0][1]
    else:
        return None

    content_url = anchor[index].attrib['href']
    content_url = base_url + content_url

    return content_url

def get_kakao_header(refer):
    headers = {
        'Accept':'*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
        'Host': 'place.map.kakao.com',
        'Connection': 'keep-alive',
        'Referer': refer,
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest'
    }
    return headers

def kakao_info_data(url):
    for i in range(len(url)-1, 0, -1):
        if url[i] == '/' or url[i] == '=':
            break
    source_id = url[i+1:len(url)]
    api_url = f'https://place.map.kakao.com/main/v/{source_id}'
    with requests.Session() as s:
        s.get(url)
        res = s.get(api_url,  params={"_": int(time.time() * 1000)}, headers = get_kakao_header(url)).text
    return res

def kakao_json(res):
    c_score = None
    c_work_time = None
    c_menu = None
    try:
        res = json.load(res)
    except:
        return c_score, c_work_time, c_menu


    time_list = res['basicInfo']
    time_list = time_list['openHour']
    time_list = time_list['periodList'][0]
    time_list = time_list['timeList']
    if len(time_list) > 0:
        time_list = time_list[0]
        c_work_time = time_list['timeSE']


    menu_list = res['menuInfo']['menuList']
    if len(menu_list) > 0:
        c_menu = []
        for elem in menu_list:
            c_menu.append(elem['menu'])
            c_menu.append(elem['price'])

    return c_score, c_work_time, c_menu


def make_restaurant_json(uuid, list):
    import ast

    score = ''
    work_time = ''
    menu = ''

    site_info_list = []
    for tup in list:
        site_name = tup[0]
        raw_data = tup[1]
        recovery = tup[2]
        recovery = ast.literal_eval(recovery)  # string form list ro list

        # recovery = [category, identify, header, url, site]
        category = recovery[0]
        identify = recovery[1]
        # identify = ast.literal_eval(identify)
        url = recovery[3]
        site = recovery[4]

        # 0 uid, 1영업상태구분코드(1이어야영업), 2전화, 3주소, 4도로명, 5가게이름, 6갱신일자, 7한식,경양식 등
        phone = identify[2]
        address = identify[3]
        road_address = identify[4]
        title = identify[5]
        food_type = identify[7]

        if site_name == 'dining_code':
            c_score, c_work_time, c_menu = get_dining_code_data(raw_data, site)
        elif site_name == 'kakao_map':
            c_score, c_work_time, c_menu = kakao_json(raw_data)
        elif site_name == 'naver_map':
            c_score = None
            c_work_time = None
            c_menu = None
        else : #mangoplate
            c_score, c_work_time, c_menu = get_data(raw_data, site)

        if c_work_time != None and len(c_work_time) > len(work_time):
            work_time = c_work_time
        if c_menu != None and len(c_menu) > len(menu):
            menu = c_menu

        tmp = OrderedDict()
        tmp['site_name'] = site_name
        tmp['url'] = url
        tmp['rating'] = c_score
        site_info_list.append(tmp)

    info_part = OrderedDict()  # poster, actor, summary
    info_part['address'] = address
    info_part['road_address'] = road_address
    info_part['phone'] = phone
    info_part['food_type'] = food_type
    info_part['work_time'] = work_time
    info_part['menu'] = menu

    data_part = OrderedDict()
    data_part['uuid'] = uuid
    data_part['category'] = category
    data_part['title'] = title
    data_part['info'] = info_part

    json_form = OrderedDict()
    json_form['data'] = data_part
    json_form['site'] = site_info_list
    j = json.dumps(json_form, ensure_ascii=False)
    return json.loads(j)

def get_dining_code_data(raw_data, site):
    soup = BeautifulSoup(raw_data, 'html.parser')

    c_score = soup.findAll('p', {'id' : 'lbl_star_point'})
    tree = html.fromstring(str(c_score))
    c_score = tree.xpath('//*[@id="lbl_star_point"]/span[1]')
    if len(c_score) > 0:
        c_score = c_score[0].text_content().strip()
        c_score = c_score[:-1]
    else :
        c_score = None

    c_time = soup.findAll('div', {'id' : 'div_hour'})
    tree = html.fromstring(str(c_time))
    c_time = tree.xpath('//*[@id="div_hour"]/div[1]/ul/li[1]/p[2]')
    if len(c_time) > 0:
        c_time = c_time[0].text_content()
    else :
        c_time = None


    c_menu = soup.find('div', {'id' : 'div_menu'})
    tree = html.fromstring(str(c_menu))
    c_menu = tree.xpath('//*[@id="div_menu"]/div/ul/li[not(@*)]/p[@*]')
    if len(c_menu) > 0:
        c_menu_list = []
        for elem in c_menu:
            c_menu_list.append(elem.text_content().strip())
    else :
        c_menu_list = None
    return c_score, c_time, c_menu_list

def get_data(page, site_data):

    tree = html.fromstring(page)
    score_xpath = site_data['score_xpath']
    time_xpath = site_data['time_xpath']
    menu_xpath = site_data['menu_xpath']

    score = tree.xpath(score_xpath)
    if len(score) > 0:
        score = extract_score(score[0].text_content())
    else :
        score = None

    work_time = tree.xpath(time_xpath)
    if len(work_time) > 0:
        work_time = work_time[0].text_content().strip()
    else :
        work_time = None

    menu = tree.xpath(menu_xpath)
    if len(menu) > 0:
        menu_list = []
        for elem in menu:
            menu_list.append(elem.text_content().strip())
    else :
        menu_list = None

    return score,work_time,menu_list

def extract_score(s):
    score = s.strip()
    for i in range(len(score)):
        if score[i] == '.':
            score = score[i-1:i+2]
            break
    return score




############## naver module #####################

def naver_header():
    headers = {
        'authority': 'map.naver.com',
        'method': 'GET',
        'path': '/search2/local.nhn?menu=location&tab=1&isFirstSearch=true&__fromRestorer=true&query=%EC%B6%98%EC%8B%AC%EC%9D%B4%EB%84%A4&searchCoord=&sm=clk&mpx=37.5032499%2C126.9506305%3AZ11%3A0.0084467%2C0.0132467',
        'scheme': 'https',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': 'NNB=G773YSG2ZMGV2; _ga=GA1.2.245713324.1563528463; nx_ssl=2; BMR=s=1571501343904&r=https%3A%2F%2Fnid.naver.com%2Fnidlogin.login%3Fsvctype%3D262144%26url%3Dhttps%3A%2F%2Fm.place.naver.com%2Fmy&r2=https%3A%2F%2Fsearch.naver.com%2Fsearch.naver%3Fsm%3Dtop_hty%26fbm%3D0%26ie%3Dutf8%26query%3D%25EB%2584%25A4%25EC%259D%25B4%25EB%25B2%2584%2B%25ED%2594%258C%25EB%25A0%2588%25EC%259D%25B4%25EC%258A%25A4; nid_inf=1198397651; NID_JKL=qIHN2v4N05Hyo5EOoZhs8wOkdjS/X6t4G4PguVn2Biw=; _naver_usersession_=bjLWjdXW1sT7oHg3OP/LBA==; PnF0E3KUyPp2RE3zV0O4iAfBuWA="GVM6mafmycuo7YSZtBnY7GE1MFU=|RQaoPbql7iuEU8jk|1571583600000"; XsVGu38lYWeWX8JGBEK4Ul2YdKk="Oi1klPIDA9paTQO4X5Id7Y4/E2M=|Jn4aw20lYzvlLHiE|1571587200000"; JSESSIONID=6D701776BB6528C9EF2240139C542FCB; page_uid=Ujx/Alpy710ssnegkB4ssssss5s-337495',
        'referer': 'https://map.naver.com/?query=%EC%B6%98%EC%8B%AC%EC%9D%B4%EB%84%A4',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest'
        }
    return headers




# :path: /search2/local.nhn?menu=location&tab=1&isFirstSearch=true&__fromRestorer=true&query=%EC%B6%98%EC%8B%AC%EC%9D%B4%EB%84%A4&searchCoord=&sm=clk&mpx=37.5032499%2C126.9506305%3AZ11%3A0.0084467%2C0.0132467

# cookie: NNB=G773YSG2ZMGV2; _ga=GA1.2.245713324.1563528463; nx_ssl=2; BMR=s=1571501343904&r=https%3A%2F%2Fnid.naver.com%2Fnidlogin.login%3Fsvctype%3D262144%26url%3Dhttps%3A%2F%2Fm.place.naver.com%2Fmy&r2=https%3A%2F%2Fsearch.naver.com%2Fsearch.naver%3Fsm%3Dtop_hty%26fbm%3D0%26ie%3Dutf8%26query%3D%25EB%2584%25A4%25EC%259D%25B4%25EB%25B2%2584%2B%25ED%2594%258C%25EB%25A0%2588%25EC%259D%25B4%25EC%258A%25A4; nid_inf=1198397651; NID_JKL=qIHN2v4N05Hyo5EOoZhs8wOkdjS/X6t4G4PguVn2Biw=; _naver_usersession_=bjLWjdXW1sT7oHg3OP/LBA==; PnF0E3KUyPp2RE3zV0O4iAfBuWA="GVM6mafmycuo7YSZtBnY7GE1MFU=|RQaoPbql7iuEU8jk|1571583600000"; XsVGu38lYWeWX8JGBEK4Ul2YdKk="Oi1klPIDA9paTQO4X5Id7Y4/E2M=|Jn4aw20lYzvlLHiE|1571587200000"; JSESSIONID=6D701776BB6528C9EF2240139C542FCB; page_uid=Ujx/Alpy710ssnegkB4ssssss5s-337495


# def naver_test(test): for api
#     title = test[4]
#     url = f'https://openapi.naver.com/v1/search/local.json?query={title}'
#
#     page = requests.get(url, headers=get_header()).text
#     print(page)

# site_name = 'naver_restaurant'
# url = 'https://map.naver.com/search2/local.nhn?menu=location&tab=1&isFirstSearch=true&__fromRestorer=true&query=<TITLE>'
#
# base_url = 'https://store.naver.com/restaurants/detail?id='

def get_naver_restaurant_url(title, address, url, base_url):
    map_url = f'https://map.naver.com/?query={title}'
    t = page = requests.get(map_url, headers=get_header()).text

    index = t.find('hmacToken:')
    index += 10
    token = ''
    while True:
        token += t[index]
        if t[index] == '}':
            break
        index += 1
    print(token)
    page = requests.get(url, headers=naver_header(), params=token).json()
    return page

# naver_api client_id : A9ukCxTDMSkVKn9v_tsb
# secert_id : gHMTylftOT
# https://map.naver.com/common2/encryptParams.nhn 파라미터

############## naver module #####################



# test = ['1','1','1','제주특별자치도 제주시 도두일동 2587-4번지','제주특별자치도 제주시 도두항길 16','도두해녀의집','1','1']
# test = ['1','1','1','제주특별자치도 서귀포시 안덕면 창천리 160-4.7번지','제주특별자치도 서귀포시 안덕면 창천중앙로24번길 16','춘심이네','1','1']
# print(get_restaurant_url(1,test))
