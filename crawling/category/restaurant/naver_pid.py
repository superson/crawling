from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
import random
from time import sleep
import csv

# Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36

def init():
    chrome_option = webdriver.ChromeOptions() #headless 옵션 객체 생성
    chrome_option.add_argument('--headless')
    chrome_option.add_argument('--no-sandbox')
    chrome_option.add_argument('--disable-dev-shm-usage')
    chrome_option.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36")

    driver = webdriver.Chrome('/Users/kimhyunseok/Downloads/chromedriver', options=chrome_option) #chrome driver 사용 및 headless 옵션 객체 적용
    driver.implicitly_wait(3) #랜더링 완료 시간 대기
    return driver

def get_restaurant_input(index):
    f = open('/Users/kimhyunseok/Documents/develope/docker/crawling/crawling/input/food/refine'+str(index)+'.csv', 'r', encoding='utf-8')
    data = csv.reader(f)
    data = list(data)
    f.close()
    return data

def get_match(title, address, candidate_title, candidate_data):
    candidate_title = candidate_title.strip()
    candidate_data = candidate_data.strip()

    for i in range(len(candidate_title)):
        if candidate_title[i] == '.':
            break
    candidate_title = candidate_title[i+2:]
    cnt = 0
    candidate_title_list = candidate_title.split(' ')
    for elem in candidate_title_list:
        if elem in title:
            cnt += 1

    identify_list = candidate_data.split(' ')
    for elem in identify_list:
        if elem in address:
            cnt += 1
    return cnt

def extract_pid(s):
    i = s.find('title,')
    i += 6
    id = ''
    for c in range(i,len(s)):
        id += s[c]
        if s[c] == ',':
            break
    return id

def remove_title(t):
    title = ''
    for c in t:
        if c == '(':
            break
        title += c
    return title

def main(index):
    driver = init()
    input_list = get_restaurant_input(index)
    print(len(input_list))
    po_f = open('/Users/kimhyunseok/Documents/develope/docker/crawling/crawling/input/food/point.txt','r',encoding='utf-8')
    point = po_f.readline()
    point = point.strip()

    po_f.close()
    print('start point :',point)
    f = open(f'/Users/kimhyunseok/Documents/develope/docker/crawling/crawling/input/food/pid_{index}_{point}.csv', 'w', encoding='utf-8', newline='')
    wr = csv.writer(f)
    wr.writerow(["가게이름", "지번주소", "도로명주소", "content_url"])

    point = int(point)
    cnt = 1
    for input in input_list:
        try:
            title = input[5]
            title = remove_title(title)
            if title == '사업장명':
                continue

            if cnt < point:
                cnt += 1
                continue

            cnt += 1
            point += 1

            if len(input[4]) > 0:
                address = input[4]
            else :
                address = input[3]

            url = f'https://map.naver.com/?query={title}'
            driver.get(url)

            title_xpath = '/html/body/app/layout/div/div[2]/div[2]/shrinkable-layout/search-layout/search-list/search-list-contents/perfect-scrollbar/div/div[1]/div/div/div/search-item-place[@*]/div/div[1]/div[1]/strong/span'

            identify_xpath = '/html/body/app/layout/div/div[2]/div[2]/shrinkable-layout/search-layout/search-list/search-list-contents/perfect-scrollbar/div/div[1]/div/div/div/search-item-place[@*]/div/div[1]/div[3]'
            a_xpath = f'//*[@id="panel"]/div[2]/div[1]/div[2]/div[2]/ul/li[{index}]/div[2]/ul/li[4]/a'

            titles = driver.find_elements_by_xpath(title_xpath) #anchor title_xpath 객체 생성
            identifys = driver.find_elements_by_xpath(identify_xpath) # check xpath
            candidate_list = []
            for i in range(len(titles)):
                candidate_title = titles[i].text
                candidate_data = identifys[i].text

                match_point = get_match(title,address,candidate_title,candidate_data)
                if match_point > 2:
                    elem = (i, match_point)
                    candidate_list.append(elem)
                else:

                    continue

            if len(candidate_list) > 0:
                candidate_list.sort(key=lambda t: t[1], reverse=True)
                index = candidate_list[0][0]
                match = candidate_list[0][1]

                val = titles[index].get_attribute("class")
                pid = extract_pid(val)
                content_url = 'https://store.naver.com/restaurants/detail?id='+pid
                wr.writerow([title, input[3], input[4], content_url])
                print(title,' - ',content_url)
            else:
                wr.writerow([title, input[3], input[4], ''])
                print(title,input[3],input[4],'search fail...')
                continue

            sleep(1.2 + random.random())
        except:
            print('end point is ',point)
            po_f = open('/Users/kimhyunseok/Documents/develope/docker/crawling/crawling/input/food/point.txt','w',encoding='utf-8')
            po_f.write(str(point))
            po_f.close()
            f.close()
            driver.quit()
            return
    f.close()
    driver.quit()

main(1)

# test = ['1','1','1','제주특별자치도 서귀포시 안덕면 창천리 160-4.7번지','제주특별자치도 서귀포시 안덕면 창천중앙로24번길 16','춘심이네','1','1']
# test = ['1','1','1','제주특별자치도 제주시 도두일동 2587-4번지','제주특별자치도 제주시 도두항길 16','도두해녀의집','1','1']
