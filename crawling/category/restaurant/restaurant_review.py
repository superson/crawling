import requests
from bs4 import BeautifulSoup
from lxml import html
import time
from time import sleep
import random
import json
import ast

def get_dining_code_header(refer):
    headers = {
    'referer': refer,
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest'
    }
    return headers

def get_dining_datas(source_id, page_index):
    datas = {
        'mode': 'LIST',
        'type': 'profile',
        'v_rid': source_id,
        'page': page_index,
        'rows': '5'
    }
    return datas

def get_dining_review(source_id):
    refer_url = f'https://www.diningcode.com/profile.php?rid={source_id}'
    review_url = 'https://www.diningcode.com/2018/ajax/review.php'
    review_list = []
    with requests.Session() as s:
        s.get(refer_url)
        datas = get_dining_datas(source_id, '1')
        page = s.post(review_url, headers=get_dining_code_header(refer_url), data=datas).text
        soup = BeautifulSoup(page, 'html.parser')

        total = soup.find('input', {'id' : 'hdn_pageT_review'})
        try:
            total = total['value']
            total = int(total)+1
        except:
            total = 1

        for index in range(1,total):
            datas = get_dining_datas(source_id, str(index))
            page = s.post(review_url, headers=get_dining_code_header(refer_url), data=datas).text
            soup = BeautifulSoup(page, 'html.parser')

            user_name_list = soup.findAll('strong')
            date_list = soup.findAll('i', {'class' : 'date'})
            comment_list = soup.findAll('p', {'class' : 'review_contents'})
            rating_list = soup.findAll('span', {'class' : 'star-date'})
            for i in range(0,len(comment_list)):
                try:
                    user_name = user_name_list[i].find(text=True)
                    date = date_list[i].find(text=True)
                    comment = ''
                    for chunk in comment_list[i].find_all(text=True):
                        comment += chunk.replace('\n',' ')
                    rating = str(rating_list[i])
                    width_index = rating.find('width:')
                    end_index = rating.find('%;')
                    rating = rating[width_index+6:end_index]
                    rating = str(int(rating)/20)
                    tmp = [user_name, comment , date, rating]
                    review_list.append(tmp)
                except:
                    continue
            sleep(1.1 + random.random())

    return review_list

def get_mangoplate_header(refer):
    header = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Host': 'stage.mangoplate.com',
        'Origin': 'https://www.mangoplate.com',
        'Referer': refer,
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    }
    return header

def get_header():
    headers = {
        'user-agent': 'user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    return headers

def get_mangoplate_review(source_id):
    url = f'https://www.mangoplate.com/restaurants/{source_id}'

    api_base = f'https://stage.mangoplate.com/api/v5/restaurants/{source_id}/reviews.json?'
    param = 'language=kor&device_uuid=spoTZ15732213561368911ny1D2&device_type=web&start_index={start_index}&request_count=5&sort_by=2'

    review_list = []
    with requests.Session() as s:
        page = s.get(url, headers=get_header()).text
        review_count_index = page.find('data-review_count')
        review_count_index += 19
        reviewCount = ''
        for i in range(review_count_index,review_count_index+10):
            if page[i] == '"':
                break
            reviewCount += page[i]

        try:
            reviewCount = int(reviewCount)
        except:
            reviewCount = 1

        for start_index in range(0,reviewCount,5):
            api_url = api_base + param.format(start_index=str(start_index))
            res = s.get(api_url, headers=get_mangoplate_header(url))
            try:
                res = res.json()
            except:
                break

            for elem in res:
                try:
                    user_name = elem['user']
                    user_name = user_name['nick_name']
                    comment = elem['comment']
                    comment = comment['comment']
                    comment = comment.replace('\n',' ')
                    date = elem['time']
                    rating = elem['action_value']
                    rating = str(int(rating)-1 + int(rating))
                    tmp = [user_name,comment,date,rating]
                    review_list.append(tmp)
                except:
                    continue
            sleep(0.8+random.random())
    return review_list

def kakao_header(refer):
    header = {
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
    'Connection': 'keep-alive',
    'Host': 'place.map.kakao.com',
    'Referer': refer,
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest'
    }
    return header

def get_kakao_review(source_id):
    print(source_id)
    url = f'https://place.map.kakao.com/{source_id}'
    base_url = f'https://place.map.kakao.com/commentlist/v/{source_id}/'
    param = '{index}?platform='
    api_url = base_url + param.format(index='1')

    review_list = []
    with requests.Session() as s:
        r = s.get(url, headers=get_header())
        print(r)
        response = s.get(api_url , headers=kakao_header(url))

        try:
            kakao_json = response.json()
            comment_part = kakao_json['comment']
            c_total = comment_part['allComntcnt']
            total = int(c_total)//5 + 2
            if int(c_total) != 0 and int(c_total)%5 == 0:
                total -= 1
        except:
            print('in total exception')
            total = 1

        print(total)
        for index in range(1,total):
            sleep(1.7+random.random()*2)
            api_url = base_url + param.format(index=str(index))
            response = s.get(api_url, headers=kakao_header(url))
            kakao_json = response.json()
            comment_part = kakao_json['comment']
            try:
                list_part = comment_part['list']
            except:
                continue

            for latter in list_part:
                try:
                    date = latter['date']
                    comment = latter['contents']
                    comment = comment.replace('\n',' ')
                except:
                    continue

                try:
                    user_name = latter['username']
                    user_name = user_name[:20]
                except:
                    user_name = ' '

                try:
                    rating = latter['point']
                except:
                    rating = ' '

                tmp = [user_name, comment, date, rating]
                review_list.append(tmp)

    return review_list
print(get_kakao_review('16103775'))
