from celery_setting import app
import executor
import logging
logging.basicConfig(filename='./log/task.log', format='%(asctime)s %(levelname)-8s %(message)s',
                    level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S')

@app.task
def movie_schedule():  # 영화 주기적 업데이트
    uuid_list = executor.get_uuid_date_condition('movie')
    input = executor.get_identify(uuid_list)
    executor.insert_rawdata(input)
    executor.update_refined(uuid_list)

@app.task
def restaurant_schedule():  # 음식점 주기적 업데이트
    uuid_list = executor.get_uuid_date_condition('restaurant')
    input = executor.get_identify(uuid_list)
    executor.insert_rawdata(input)
    executor.update_refined(uuid_list)
