FROM python:3.7-slim-buster

WORKDIR /app
ADD . /app

RUN apt-get update
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y make
RUN apt-get install -y --reinstall build-essential

RUN apt-get install -y vim
RUN apt-get install -y libpq-dev python-dev
RUN pip3 install --upgrade pip
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 9876
